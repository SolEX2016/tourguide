﻿using System.Web;
using System.Web.Optimization;

namespace Tour
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/js/jquery-1.11.1.min.js",
                     "~/Content/js/jquery.noconflict.js",
                     "~/Content/js/modernizr.2.7.1.min.js",
                     "~/Content/js/jquery-migrate-1.2.1.min.js",
                     "~/Content/js/query.placeholder.js",
                     "~/Content/js/jquery-ui.1.10.4.min.js",
                     "~/Content/js/bootstrap.js",
                     "~/Content/components/revolution_slider/js/jquery.themepunch.plugins.min.js",
                        "~/Content/components/jquery.bxslider/jquery.bxslider.min.js",
                     "~/Content/components/flexslider/jquery.flexslider-min.js",
                     "~/Content/js/gmap3.min.js",
                     "~/Content/js/jquery.stellar.min.js",
                     "~/Content/js/waypoints.min.js",
                     "~/Content/js/theme-scripts.js",
                     "~/Content/js/scripts.js",
                     "~/Content/components/revolution_slider/js/jquery.themepunch.revolution.min.js",
                     "~/Content/components/revolution_slider/js/jquery.themepunch.revolution.min.js",
                     "~/Content/components/revolution_slider/js/jquery.themepunch.revolution.min.js",
                      "~/Content/components/revolution_slider/js/jquery.themepunch.revolution.min.js" ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/bootstrap.min.css",
                       "~/Content/css/font-awesome.min.css",
                        "~/Content/css/animate.min.css",
                         "~/Content/components/revolution_slider/css/settings.css",
                          "~/Content/components/revolution_slider/css/style.css",
                           "~/Content/components/jquery.bxslider/jquery.bxslider.css",
                            "~/Content/components/flexslider/flexslider.css",
                             "~/Content/css/style.css",
                              "~/Content/css/custom.css",
                               "~/Content/css/updates.css",
                                "~/Content/css/responsive.css"));
        }
    }
}
